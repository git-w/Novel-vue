import {GetterTree} from 'vuex';

const getters: GetterTree<any, any> = {
  collapse: (state) => state.collapse,
  projectInfo: (state) => state.projectInfo,
  logoSrc: (state) => state.logoSrc,
  browserHeaderTitle: (state) => state.browserHeaderTitle,
  visitedViews: (state) => state.visitedViews,
  cachedViews: (state) => state.cachedViews,
  device: (state) => state.device,
};

export default getters;
