import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import ElTreeSelect from 'el-tree-select';
import eIconPicker from 'e-icon-picker';

import "e-icon-picker/lib/symbol.js"; //基本彩色图标库
import 'e-icon-picker/lib/index.css'; // 基本样式，包含基本图标
import 'font-awesome/css/font-awesome.min.css'; //font-awesome 图标库
import axios from '@/utils/http';
import VueCropper from 'vue-cropper';
// collapse 展开折叠
import CollapseTransition from 'element-ui/lib/transitions/collapse-transition';
import 'element-ui/lib/theme-chalk/index.css';
// fade/zoom 等
// 过渡动画 css
import '@/assets/css/index.scss';
import '@/router/permission';
import '@/directive/permission';
import utils from '@/utils';
import horizontalScroll from '@/directive/horizontalScroll';
import Table from "@/components/Table/index.vue";
import resetForm = utils.resetForm;
import download = utils.download;

Vue.config.productionTip = false;
Vue.component("el-table", Table);
Vue.use(VueCropper);
Vue.prototype.resetForm = resetForm;
Vue.prototype.$download = download;
Vue.prototype.$axios = axios;
Vue.component(CollapseTransition.name, CollapseTransition);
Vue.use(ElementUI);
Vue.use(ElTreeSelect);
Vue.use(horizontalScroll);
//全局删除增加图标
Vue.use(eIconPicker, {
  FontAwesome: true,
  ElementUI: true,
  eIcon: true,//自带的图标，来自阿里妈妈
  eIconSymbol: true,//是否开启彩色图标
  zIndex: 3000//选择器弹层的最低层,全局配置
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
