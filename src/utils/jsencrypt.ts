import {JSEncrypt} from 'encryptlong';

// 密钥对生成 http://web.chacuo.net/netrsakeypair

const publicKey = `MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDNyR4MJsMoC1UQTVA6911zzVUn
PM0B42/IOIQR28GIBqXR1XQA2782YDGuWmJ63/O5XAVpYQCn62PmorFjDdxu3emE
cnMaCzcrKtDVS9g+XYYKw5tvUI8iH0zBEXx9bYGnDM/dRVivtmMXH0/qP6BxFUhr
1F1qH5wowSQM9DO83QIDAQAB`;

const privateKey = `MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAM3JHgwmwygLVRBN
UDr3XXPNVSc8zQHjb8g4hBHbwYgGpdHVdADbvzZgMa5aYnrf87lcBWlhAKfrY+ai
sWMN3G7d6YRycxoLNysq0NVL2D5dhgrDm29QjyIfTMERfH1tgacMz91FWK+2Yxcf
T+o/oHEVSGvUXWofnCjBJAz0M7zdAgMBAAECgYEAwiiBiyF/fGbEyW7IxfniWIJh
V3YMTz3Rl+2K3najML5k8YgdvTmYPaug9EofJKPshvUFvhlhzw1O4qqaB0VqthY8
vsHWsCnQ9BuQqFw6o0U79vsYqBnS5D80qX4fIjQkD4Js9m4iGyvFdrJ6ykwQAE7y
a71KdAFChAiNhSydHAECQQDyBDOsl0qPwAEasVKqengKxqrZXm+mRftCMHB+DAu8
c7Z7GdRIoBsuF/c+G2dvg2PdVd3AocPcJx+Ru4FCGJhdAkEA2a0BfLra6kqzI95H
Ze05OoO010lMVHLgKIxNUY0Ccx4BPDt1kdRJ63JWeLXvpRJTwyB9qrbImlKtA3NR
xENugQJAL7JMTNN0SmhYz13rINYKGnWU7koCTw6zuhO5192f4oAbkp9Aix+iu67V
d1QHSp7ma5Sko3Y+F4FOY3ZEvzYBsQJBAMKjcSX/ywoeu2pCMqOQaFDLPiQXFZqZ
kZiQQ/680MjwtngOTsWUBnjypyGOxXdcGOvsnJHOmTds3+mq7x1IkoECQQCZkVC3
ky8MNKlmISXcIVKBMrk+A5HU3732uW8E5c+817BHjhsy3q2TplJAB+R2TaIDPksH
/h7ot83nFxP0wX4Y`;


/**
 * 加密
 * @param txt 需要加密的字符串
 */
export const encrypt = (txt: string): string => {
  const encryptor = new JSEncrypt();
  encryptor.setPublicKey(publicKey); // 设置公钥
  return encryptor.encryptLong(txt); // 对需要加密的数据进行加密
}

/**
 * 解密
 * @param txt 需要解密的字符串
 */
export const decrypt = (txt: string): string => {
  const encryptor = new JSEncrypt();
  encryptor.setPrivateKey(privateKey);
  return encryptor.decryptLong(txt);
}

