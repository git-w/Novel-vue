import http from '@/utils/http';

/**
 * 获取部门树
 * @param params
 * @returns {*|Promise<any>}
 */
export const deptTreeSelectData = (params?: any) => {
  return http.get('system/dept/deptTreeSelectData', {params});
}

/**
 * 获取部门树型表格数据
 * @param params
 * @returns {*|Promise<any>}
 */
export const getDeptTreeTableData = (params: any = {}) => {
  return http.get('system/dept/deptTreeTableData', {params});
}

/**
 * 删除部门信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeDept = (params: any) => {
  return http.delete('system/dept/remove', {params});
}

/**
 * 保存修改部门信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateDept = (params: any) => {
  return http.put('system/dept/edit', params);
}

/**
 * 保存新增部门信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addDept = (params: any) => {
  return http.post('system/dept/add', params);
}

/**
 * 通过id获取部门信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const getById = (params: any) => {
  return http.get('system/dept/' + params.id);
}

/**
 * 验证部门名称是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkDeptNameUnique = (params: any) => {
  params.loading = false;
  return http.post('system/dept/checkDeptNameUnique', params);
}

