import http from '@/utils/http';

/**
 * 获取参数配置列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getConfigList = (params?: any) => {
  return http.get('system/config/list', {params: params});
}


/**
 * 删除参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeConfig = (params: any) => {
  return http.delete('system/config/remove', {params: params});
}

/**
 * 保存参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateConfig = (params: any) => {
  return http.put('system/config/edit', params);
}

/**
 * 新增参数配置
 * @param params
 * @returns {*|Promise<any>}
 */
export const addConfig = (params: any) => {
  return http.post("system/config/add", params);
}

/**
 * 验证参数key是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkConfigKeyUnique = (params: any) => {
  params.loading = false;
  return http.post('system/config/checkConfigKeyUnique', params);
}

/**
 * 导出参数信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportConfig = (params?: any) => {
  if (!params) {
    params = {};
  }
  return http.get('system/config/export', {params});
}

/**
 * 根据参数键名查询参数值
 * @param params
 * @returns {*|Promise<any>}
 */
export const getConfigKey = (params: any) => {
  return http.get('system/config/configKey/' + params.configKey);
}
