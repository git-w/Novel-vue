import http from '@/utils/http';

/**
 * 获取验证码
 */
export const getKaptchaImage = () => {
  return http.get('common/captchaImage');
}

/**
 * 获取系统信息
 */
export const getProjectInfo = () => {
  return http.get('common/getProjectInfo?_' + new Date().getTime());
}
