import http from '@/utils/http';

/**
 * 获取系统岗位列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getPostList = (params: any) => {
  return http.get('system/post/list', {params});
}

/**
 * 删除岗位信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removePost = (params: any) => {
  return http.delete('system/post/remove', {params});
}


/**
 * 保存新增岗位信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addPost = (params: any) => {
  return http.post('system/post/add', params);
}

/**
 * 保存修改岗位信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updatePost = (params: any) => {
  return http.put('system/post/edit', params);
}

/**
 * 导出岗位信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportPost = (params?: any) => {
  return http.get('system/post/export', {params});
}


/**
 * 验证岗位名称是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkPostNameUnique = (params: any) => {
  params.loading = false;
  return http.post('system/post/checkPostNameUnique', params);
}

/**
 * 验证岗位编码是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkPostCodeUnique = (params: any) => {
  params.loading = false;
  return http.post('system/post/checkPostCodeUnique', params);
}
