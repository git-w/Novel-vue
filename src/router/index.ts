import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/views/Login.vue';
import Layout from '@/layout/Layout.vue';

Vue.use(Router);
// 公共路由
export const constantRoutes = [
  {
    path: '',
    component: Layout,
    redirect: 'home',
    visible: false,
    children: [
      {
        path: 'home',
        component: () => import('@/views/Home.vue'),
        name: '首页',
        visible: true,
        meta: {title: '首页', icon: 'el-icon-monitor', cache: true, affix: true},
      },
    ],
  },
  {
    path: '/login',
    name: '用户登录',
    visible: false,
    component: Login,
  },
  {
    path: '/user',
    name: '个人中心',
    visible: false,
    component: Layout,
    children: [
      {
        path: 'profile',
        visible: false,
        name: '个人信息',
        component: () => import('@/views/Profile.vue'),
        meta: {
          title: '个人信息',
          icon: '',
          cache: false
        },
      },
    ],
  },
  {
    path: '/redirect*',
    component: Layout,
    visible: false,
    children: [
      {
        path: '/redirect/:path*',
        visible: false,
        meta: {title: 'redirect', cache: false},
        component: () => import('@/views/Redirect.vue')
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    visible: false,
    meta: {title: '404', cache: false},
    component: () => import('@/views/404.vue'),
  },
];
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: constantRoutes,
});


export default router;
